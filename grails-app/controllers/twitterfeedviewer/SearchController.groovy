package twitterfeedviewer

import grails.converters.JSON

class SearchController {

	//final String TWITTER_SEARCH_API_URL = "https://api.twitter.com/1.1/search/tweets.json?q=";

	def searchService
	
	def index() {
		def query = java.net.URLEncoder.encode(params.query, "UTF-8") 
		def results
		println " --- " + query
		//Query mongodb to find if the keyword is already a searched word
		TwitterFeed usedSearch = searchService.findTwitterFeedByQuery(query)//TwitterFeed.findByQuery(query)
		//If not found, its a new search, hit twitter api and store the results in mongo
		if(!usedSearch) {
			results = searchService.fetchResultsFromTwitterAndSaveToMongo(query);
		}else {
			results = JSON.use('deep') { usedSearch as JSON }
		}

		request.withFormat {
			html {
				redirect action:"/", method:"GET"
			}
			json { render results }
		}
	}
}