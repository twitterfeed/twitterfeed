<!doctype html>
<html>
<head>
<meta name="layout" content="custom" />
<title>Twitter Feed Viewer</title>
</head>
<body>
	<g:form name="myForm" action="myaction" controller="search">
		<div class="main">
			<g:textField name="query" value="" size="56"
				placeholder="Enter text or hashtag" />
			<g:submitButton name="handleSearch" id="handleSearch" value="SUBMIT" />
			<div id="spinner" class="spinner" style="display: none;">
				<g:message code="spinner.alt" default="Loading&hellip;" />
			</div>
			<table class="searchResults">
				<thead style="background-color: black">
					<tr>
						<th width="20%">User Name</th>
						<th>Tweeted Text</th>
					</tr>
				</thead>
				<tbody class="resultsFetched">
					<tr>
						<td align="center" colspan="2">No Matching Records.</td>
					</tr>
				</tbody>
			</table>
			<p>N O T E : The results are limited to 15 records.</p>
		</div>
	</g:form>
</body>
</html>
