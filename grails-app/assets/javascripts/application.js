$(document).ready(function(){
	//On Button click, load the results
	$('#handleSearch').on("click", function(event) {
		console.log($('#query').val())
		if($('#query').val().length > 0) {
			$.ajax({
				type:"GET",
				contentType: "application/json",
				url: "/search?query="+encodeURIComponent($('#query').val()),
				beforeSend: function(){
			        $('#spinner').fadeIn();
			    },
			    complete: function(){
			        $('#spinner').fadeOut();
			    },
				success: function(data){
					$('.resultsFetched').html("")
					var noMatchingRecords = true;
					for(tweet in data.feedDetails) {
						noMatchingRecords = false;
						var tr = "<tr>"+
									"<td>"+data.feedDetails[tweet].userName+"</td>"+
									"<td>"+data.feedDetails[tweet].tweetText+"</td>"+
								  "</tr>";
						$('.resultsFetched').append(tr)
					}
					if(noMatchingRecords){
						$('.resultsFetched').append("<tr><td align='center' colspan='2'>No Matching Records.</td></tr>");
					}
				}
			})
			$('#query').removeClass("error");
		}else { //If no text entered, highlights textbox with red color border
			$('#query').addClass("error");
		}
		event.preventDefault();
		return false;
	});	
});
