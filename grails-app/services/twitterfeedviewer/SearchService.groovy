package twitterfeedviewer

import grails.gorm.transactions.Transactional
import grails.converters.JSON
import groovy.json.JsonSlurper
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.grails.web.json.JSONObject

@Transactional
class SearchService {

	final String TWITTER_SEARCH_API_URL = "https://api.twitter.com/1.1/search/tweets.json?q=";
	 
    def findTwitterFeedByQuery(String query) {
		TwitterFeed tf = TwitterFeed.findByQuery(query)
		return tf;
    }
	
	def fetchResultsFromTwitterAndSaveToMongo(String query) {
		def results
		URL url = new URL(TWITTER_SEARCH_API_URL + query);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setRequestProperty("Authorization","Bearer "+"AAAAAAAAAAAAAAAAAAAAAAVNAgEAAAAAR9lL2baoVd7%2F1U3FFDofeuGCu74%3DxAYd472QhTt8NkeTUphAN0R7tH2kseIFDzJq4P8uiNmINk8ysK");
		conn.setRequestProperty("Content-Type","application/json");
		conn.setRequestMethod("GET");

		BufferedReader inr = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String output;
		StringBuffer response = new StringBuffer();
		while ((output = inr.readLine()) != null) {
			response.append(output);
		}
		inr.close();

		def jsonSlurper = new JsonSlurper()
		results = jsonSlurper.parseText(response.toString())

		//Create TwitterFeed object with keyword
		TwitterFeed tf = new TwitterFeed(query: query)

		results.statuses.each { tweet ->
			TwitterFeedDetails tfd = new TwitterFeedDetails(userName: tweet.user.name, tweetText: tweet.text)
			tfd.save(failOnError:true)
			tf.addToFeedDetails(tfd)
		}
		tf.save(failOnError:true);

		//Convert the received object and its child objects to JSON
		results = JSON.use('deep') { tf as JSON }
		return results
	}
}
