package twitterfeedviewer

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration

import com.mongodb.MongoClient
import com.mongodb.MongoClientURI
import com.mongodb.client.MongoDatabase

class Application extends GrailsAutoConfiguration {
    static void main(String[] args) {
        GrailsApp.run(Application, args);		
    }
}