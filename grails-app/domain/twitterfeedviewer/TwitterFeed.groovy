package twitterfeedviewer

class TwitterFeed {

	String query
	static hasMany = [feedDetails : TwitterFeedDetails]
	
    static constraints = {
    }
	
	static mapping = {
		collection "twitter_feed"
	}
}
