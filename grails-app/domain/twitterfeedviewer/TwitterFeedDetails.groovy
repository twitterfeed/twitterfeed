package twitterfeedviewer

class TwitterFeedDetails {

	String userName
	String tweetText
	
    static constraints = {
    }
	
	static mapping = {
		collection "twitter_feed_details"
		userName attr: "user_name"
		tweetText attr: "tweet_text"		
	}
}
