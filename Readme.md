------------------------------------------------------------------
Instructions to setup
------------------------------------------------------------------

1) Clone the repo
2) Update application.yml file by giving mongo db creds
3) Pass the bearer token in SearchController.groovy
4) Run the app. Ex: grails -Dgrails.reload.enabled=true run-app